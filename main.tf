terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "=3.42.0"
    }
  }
}

provider "aws" {
  region = var.region
}

resource "aws_vpc" "hashicat" {
  cidr_block           = var.address_space
  enable_dns_hostnames = true

  tags = {
    name        = "${var.prefix}-vpc-${var.region}"
    environment = "Production"
    yor_trace   = "201b8131-387b-41ef-b5ed-3af6edae6c1a"
  }
}

resource "aws_subnet" "hashicat" {
  vpc_id     = aws_vpc.hashicat.id
  cidr_block = var.subnet_prefix

  tags = {
    name                 = "${var.prefix}-subnet"
    git_commit           = "5abbdc0f9682e1cf70cebd7f500dcac3e07f7cd5"
    git_file             = "main.tf"
    git_last_modified_at = "2020-09-17 23:05:03"
    git_last_modified_by = "roger@hashicorp.com"
    git_modifiers        = "go/kcorbin/roger/scarolan"
    git_org              = "jroberts2600"
    git_repo             = "hashicat-aws"
    yor_trace            = "3fb6a776-867e-4ab7-93a3-1e6446a5a445"
  }
}

resource "aws_security_group" "hashicat" {
  name = "${var.prefix}-security-group"

  vpc_id = aws_vpc.hashicat.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name                 = "${var.prefix}-security-group"
    git_commit           = "5abbdc0f9682e1cf70cebd7f500dcac3e07f7cd5"
    git_file             = "main.tf"
    git_last_modified_at = "2020-09-17 23:05:03"
    git_last_modified_by = "roger@hashicorp.com"
    git_modifiers        = "kcorbin/roger/scarolan"
    git_org              = "jroberts2600"
    git_repo             = "hashicat-aws"
    yor_trace            = "ba971ef3-4827-4086-8562-440928e63a29"
  }
}

resource "random_id" "app-server-id" {
  prefix      = "${var.prefix}-hashicat-"
  byte_length = 8
}

resource "aws_internet_gateway" "hashicat" {
  vpc_id = aws_vpc.hashicat.id

  tags = {
    Name                 = "${var.prefix}-internet-gateway"
    git_commit           = "5abbdc0f9682e1cf70cebd7f500dcac3e07f7cd5"
    git_file             = "main.tf"
    git_last_modified_at = "2020-09-17 23:05:03"
    git_last_modified_by = "roger@hashicorp.com"
    git_modifiers        = "kcorbin/roger/scarolan"
    git_org              = "jroberts2600"
    git_repo             = "hashicat-aws"
    yor_trace            = "6bd57c8b-7e0d-42cc-a26b-991c0343848d"
  }
}

resource "aws_route_table" "hashicat" {
  vpc_id = aws_vpc.hashicat.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.hashicat.id
  }
  tags = {
    git_commit           = "5abbdc0f9682e1cf70cebd7f500dcac3e07f7cd5"
    git_file             = "main.tf"
    git_last_modified_at = "2020-09-17 23:05:03"
    git_last_modified_by = "roger@hashicorp.com"
    git_modifiers        = "roger/scarolan"
    git_org              = "jroberts2600"
    git_repo             = "hashicat-aws"
    yor_trace            = "70180617-93ca-4693-b611-29ed244e493f"
  }
}

resource "aws_route_table_association" "hashicat" {
  subnet_id      = aws_subnet.hashicat.id
  route_table_id = aws_route_table.hashicat.id
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name = "name"
    #values = ["ubuntu/images/hvm-ssd/ubuntu-disco-19.04-amd64-server-*"]
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_eip" "hashicat" {
  instance = aws_instance.hashicat.id
  vpc      = true
  tags = {
    git_commit           = "75534608e6d974ba3b7b5aa29680d4797b1aed75"
    git_file             = "main.tf"
    git_last_modified_at = "2020-04-06 17:02:25"
    git_last_modified_by = "scarolan@hashicorp.com"
    git_modifiers        = "scarolan"
    git_org              = "jroberts2600"
    git_repo             = "hashicat-aws"
    yor_trace            = "e40bfd35-3c95-44de-956f-746e5b7c5444"
  }
}

resource "aws_eip_association" "hashicat" {
  instance_id   = aws_instance.hashicat.id
  allocation_id = aws_eip.hashicat.id
}

resource "aws_instance" "hashicat" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.hashicat.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.hashicat.id
  vpc_security_group_ids      = [aws_security_group.hashicat.id]

  tags = {
    Name      = "${var.prefix}-hashicat-instance"
    Defender  = "true"
    yor_trace = "4a285bce-d46a-44b9-ae96-3956cee5f68b"
  }
}

# We're using a little trick here so we can run the provisioner without
# destroying the VM. Do not do this in production.

# If you need ongoing management (Day N) of your virtual machines a tool such
# as Chef or Puppet is a better choice. These tools track the state of
# individual files and can keep them in the correct configuration.

# Here we do the following steps:
# Sync everything in files/ to the remote VM.
# Set up some environment variables for our script.
# Add execute permissions to our scripts.
# Run the deploy_app.sh script.
resource "null_resource" "configure-cat-app" {
  depends_on = [aws_eip_association.hashicat]

  triggers = {
    build_number = timestamp()
  }

  provisioner "file" {
    source      = "files/"
    destination = "/home/ubuntu/"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = tls_private_key.hashicat.private_key_pem
      host        = aws_eip.hashicat.public_ip
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt -y update",
      "sleep 15",
      "sudo apt -y update",
      "sudo apt -y install apache2",
      "sudo systemctl start apache2",
      "sudo chown -R ubuntu:ubuntu /var/www/html",
      "chmod +x *.sh",
      "PLACEHOLDER=${var.placeholder} WIDTH=${var.width} HEIGHT=${var.height} PREFIX=${var.prefix} ./deploy_app.sh",
      "sudo apt -y install cowsay",
      "cowsay Mooooooooooo!",
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = tls_private_key.hashicat.private_key_pem
      host        = aws_eip.hashicat.public_ip
    }
  }
}

resource "tls_private_key" "hashicat" {
  algorithm = "RSA"
}

locals {
  private_key_filename = "${random_id.app-server-id.dec}-ssh-key.pem"
}

resource "aws_key_pair" "hashicat" {
  key_name   = local.private_key_filename
  public_key = tls_private_key.hashicat.public_key_openssh
  tags = {
    git_commit           = "5abbdc0f9682e1cf70cebd7f500dcac3e07f7cd5"
    git_file             = "main.tf"
    git_last_modified_at = "2020-09-17 23:05:03"
    git_last_modified_by = "roger@hashicorp.com"
    git_modifiers        = "roger/scarolan"
    git_org              = "jroberts2600"
    git_repo             = "hashicat-aws"
    yor_trace            = "2d05f88c-cac9-4ff3-a238-624858c377d5"
  }
}
